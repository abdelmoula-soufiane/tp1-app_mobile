package com.example.tp1;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class Holder extends RecyclerView.ViewHolder {
    TextView name;
    ImageView icon;
    public Holder(@NonNull View itemView) {
        super(itemView);
        name=(TextView) itemView.findViewById(R.id.name);
        icon=(ImageView) itemView.findViewById(R.id.icon);
    }
}
