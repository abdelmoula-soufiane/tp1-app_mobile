package com.example.tp1;


import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

public class Adapter extends RecyclerView.Adapter<Holder> {
Context c;
String[] souf;
public Adapter(Context c, String[] souf){
    this.c=c;
    this.souf=souf;
}
    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
    View v= LayoutInflater.from(c).inflate(R.layout.model,parent,false);
        return new Holder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, final int i) {
    holder.name.setText(souf[i]);

    int resourceId = c.getResources().getIdentifier(
                AnimalList.getAnimal(souf[i]).getImgFile(), "drawable", c.getPackageName() );
    holder.icon.setImageResource(resourceId);

    holder.name.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(c,AnimalActivity.class);
                intent.putExtra("animalName", souf[i]);
                c.startActivity(intent);
            }

        });

    }

    @Override
    public int getItemCount() {
        return souf.length;
    }
}
