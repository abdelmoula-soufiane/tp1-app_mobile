package com.example.tp1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.content.Intent;


public class AnimalActivity extends AppCompatActivity {
    String name;
    TextView textView10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal);
        /*nv intent recupiri lintent envoyé donc intent l9dim*/
        Intent intent = getIntent();
        name = intent.getStringExtra("animalName");

        ImageView image = findViewById(R.id.imageView4);
        int resourceId = getResources().getIdentifier(
                AnimalList.getAnimal(name).getImgFile(), "drawable", getPackageName() );
        image.setImageResource(resourceId);

        TextView textView = findViewById(R.id.textView);
        textView.setText(name);
        TextView textView6 = findViewById(R.id.textView6);
        textView6.setText(AnimalList.getAnimal(name).getStrHightestLifespan());
        TextView textView7 = findViewById(R.id.textView7);
        textView7.setText(AnimalList.getAnimal(name).getStrGestationPeriod());
        TextView textView8 = findViewById(R.id.textView8);
        textView8.setText(AnimalList.getAnimal(name).getStrBirthWeight());
        TextView textView9 = findViewById(R.id.textView9);
        textView9.setText(AnimalList.getAnimal(name).getStrAdultWeight());
        textView10 = findViewById(R.id.textView10);
        textView10.setText(AnimalList.getAnimal(name).getConservationStatus());
    }

    public void save(View v){
        AnimalList.getAnimal(name).setConservationStatus(textView10.getText().toString());
    }
}
